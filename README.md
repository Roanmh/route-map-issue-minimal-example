# Route-Map Issue Minimal Example

An attempt to demonstrate how route maps may not create linking buttons on files.

## Tests

1. Introduce route maps in MRs

   Add route maps in MRs. This shows the "View App" button with the expected
   pages, but does not show the per-file buttons. The merged versions can be
   seen at !2 and !3

2. New MRs once route maps are merged.

   The changed file does not have a link like expected in !4.

